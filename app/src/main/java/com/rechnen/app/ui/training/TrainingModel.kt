/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.rechnen.app.data.Database
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus

class TrainingModel: ViewModel() {
    companion object {
        private val nullStringLive = MutableLiveData<String>().apply { postValue(null) }
    }

    private val input = Channel<TrainingInputEvent>(Channel.CONFLATED)
    private val job = Job()
    private val scope = GlobalScope + job
    private val stateInternal = MutableLiveData<TrainingState>()
    private val currentNumberInputInternal = MutableLiveData<InputState>()
    private var didInit = false

    val state: LiveData<TrainingState> = stateInternal
    val currentNumberInput: LiveData<String> = Transformations.map(currentNumberInputInternal) { it.toString() }
    val currentSpecialButtonText: LiveData<String?> = Transformations.switchMap(state) { state ->
        if (state is TrainingState.ShowTask) {
            val task = state.task

            Transformations.map(currentNumberInputInternal) { currentNumberInput ->
                when (currentNumberInput.specialInputType) {
                    SpecialInputType.Nothing -> null
                    SpecialInputType.NextItem -> task.separator
                    SpecialInputType.TogglePrefix -> task.prefix
                }
            }
        } else {
            nullStringLive
        }
    }

    fun init(
            database: Database,
            userId: Int,
            mode: TrainingMode
    ) {
        if (didInit) {
            return
        }

        didInit = true

        scope.launch {
            TrainingUtil.start(
                    input = input,
                    database = database,
                    userId = userId,
                    scope = scope,
                    mode = mode
            ).consumeEach {
                stateInternal.postValue(it)

                if (it is TrainingState.ShowTask && it.clearInput) {
                    currentNumberInputInternal.postValue(InputState.newInstance(
                            prefix = it.task.prefix,
                            separator = it.task.separator,
                            maxListLength = it.task.maxListLength
                    ))
                }
            }
        }
    }

    fun handleNumberButton(digit: Int) {
        currentNumberInputInternal.value = currentNumberInputInternal.value?.handleDigit(digit)
    }

    fun handleSpecialButton() {
        currentNumberInputInternal.value = currentNumberInputInternal.value?.handleSpecial()
    }

    fun handleDeleteButton() {
        currentNumberInputInternal.value = currentNumberInputInternal.value?.handleDelete()
    }

    fun handleOkButton() {
        val value = currentNumberInputInternal.value?.result

        input.offer(
                if (value == null)
                    TrainingInputEvent.Result(values = emptyList(), prefixEnabled = false)
                else
                    TrainingInputEvent.Result(values = value.items, prefixEnabled = value.prefixSet)
        )
    }

    fun handleContinueButton() {
        input.offer(TrainingInputEvent.Continue)
    }

    override fun onCleared() {
        super.onCleared()

        job.cancel()
    }
}