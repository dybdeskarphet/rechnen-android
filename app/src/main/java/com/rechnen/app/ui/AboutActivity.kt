/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import com.rechnen.app.BuildConfig
import com.rechnen.app.R
import com.rechnen.app.databinding.AboutActivityBinding

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = AboutActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.version.text = getString(R.string.about_version, BuildConfig.VERSION_NAME)

        binding.license.movementMethod = LinkMovementMethod.getInstance()
        binding.containedSoftwareText.movementMethod = LinkMovementMethod.getInstance()
        binding.sourceCodeUrl.movementMethod = LinkMovementMethod.getInstance()
    }
}
