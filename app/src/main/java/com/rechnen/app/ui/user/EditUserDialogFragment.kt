/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rechnen.app.databinding.EditUserDialogBinding
import com.rechnen.app.ui.training.TrainingMainActivity
import com.rechnen.app.ui.training.TrainingMode

class EditUserDialogFragment: BottomSheetDialogFragment() {
    companion object {
        private const val DIALOG_TAG = "EditUserDialogFragment"
        private const val ARG_USER_ID = "userId"
        private const val ARG_USER_TItLE = "userTitle"

        fun newInstance(userId: Int, userTitle: String) = EditUserDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_USER_ID, userId)
                putString(ARG_USER_TItLE, userTitle)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val userId = arguments!!.getInt(ARG_USER_ID)
        val userTitle = arguments!!.getString(ARG_USER_TItLE)!!

        val binding = EditUserDialogBinding.inflate(inflater, container, false)

        binding.username = userTitle

        binding.timeTrialButton.setOnClickListener {
            TrainingMainActivity.launch(requireContext(), userId, TrainingMode.TimeTrail)

            dismiss()
        }

        binding.difficultyButton.setOnClickListener {
            EditUserDifficultyDialog.newInstance(userId).show(parentFragmentManager)

            dismiss()
        }

        binding.keyboardConfigButton.setOnClickListener {
            EditUserInputConfigurationDialog.newInstance(userId = userId).show(parentFragmentManager)

            dismiss()
        }

        binding.statisticButton.setOnClickListener {
            ViewUserStatisticDialogFragment.newInstance(userId = userId).show(parentFragmentManager)

            dismiss()
        }

        binding.deleteButton.setOnClickListener {
            DeleteUserDialogFragment.newInstance(
                    userId = userId,
                    userTitle = userTitle
            ).show(parentFragmentManager)

            dismiss()
        }

        return binding.root
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}