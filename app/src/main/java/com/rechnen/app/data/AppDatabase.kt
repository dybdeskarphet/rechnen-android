/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data

import android.content.Context
import androidx.room.Room

object AppDatabase {
    private val lock = Object()
    private var instance: Database? = null

    fun with(context: Context): Database {
        if (instance == null) {
            synchronized(lock) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context, Database::class.java, "data")
                            .addMigrations(*DatabaseMigrations.ALL)
                            .build()
                }
            }
        }

        return instance!!
    }
}