/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.userlist

import android.content.Intent
import android.os.Bundle

import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.rechnen.app.R
import com.rechnen.app.data.AppDatabase

import com.rechnen.app.data.model.User
import com.rechnen.app.databinding.ActivitySelectUserBinding
import com.rechnen.app.ui.AboutActivity
import com.rechnen.app.ui.training.TrainingMainActivity
import com.rechnen.app.ui.training.TrainingMode
import com.rechnen.app.ui.user.CreateUserDialogFragment
import com.rechnen.app.ui.user.EditUserDialogFragment

class SelectUserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivitySelectUserBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val adapter = UserListAdapter()
        val database = AppDatabase.with(this)

        binding.recycler.layoutManager = LinearLayoutManager(this)
        binding.recycler.adapter = adapter

        adapter.listener = object: UserListAdapterListener {
            override fun onItemClicked(user: User) {
                startTraining(user.id)
            }

            override fun onItemLongClicked(user: User): Boolean {
                editUser(user.id, user.name)

                return true
            }
        }

        database.user().getAllUsers().observe(this, Observer {
            binding.flipper.displayedChild = if (it.isEmpty()) 1 else 0

            adapter.data = it
        })

        binding.createUserBtn.setOnClickListener { createUser() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_select_user, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add_user -> {
                createUser()
                return true
            }
            R.id.about -> {
                showAboutScreen()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createUser() {
        CreateUserDialogFragment().show(supportFragmentManager)
    }

    private fun showAboutScreen() {
        startActivity(Intent(this, AboutActivity::class.java))
    }

    private fun editUser(userId: Int, userName: String) {
        EditUserDialogFragment.newInstance(
                userId = userId,
                userTitle = userName
        ).show(supportFragmentManager)
    }

    private fun startTraining(userId: Int) {
        TrainingMainActivity.launch(this@SelectUserActivity, userId, TrainingMode.Regular)
    }
}
