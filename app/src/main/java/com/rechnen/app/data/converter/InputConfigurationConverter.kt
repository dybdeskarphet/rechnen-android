/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.converter

import androidx.room.TypeConverter
import com.rechnen.app.data.modelparts.InputConfiguration
import com.rechnen.app.extension.useJsonReader
import com.rechnen.app.extension.writeJson

class InputConfigurationConverter {
    @TypeConverter
    fun fromString(input: String): InputConfiguration = input.useJsonReader { InputConfiguration.parse(it) }

    @TypeConverter
    fun toString(configuration: InputConfiguration): String = writeJson { configuration.serialize(it) }
}