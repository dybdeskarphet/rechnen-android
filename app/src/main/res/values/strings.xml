<?xml version="1.0" encoding="utf-8"?>
<!--
    Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see https://www.gnu.org/licenses/
-->
<resources>
    <string name="app_name">Calculate</string>

    <string name="generic_cancel">Cancel</string>
    <string name="generic_save">Save</string>

    <string name="user_list_empty_text">To use this application,
        you must create a user. This is only saved locally at this device.
    </string>

    <string name="user_list_statistic">Last usage: %1$s - %2$s</string>
    <string name="user_list_statistic_empty">never used</string>
    <string name="user_list_playtime">Played %s</string>

    <string name="time_less_than_one_minute">less than one minute</string>
    <string name="time_and">%s and %s</string>
    <plurals name="time_minute">
        <item quantity="one">%d minute</item>
        <item quantity="other">%d minutes</item>
    </plurals>
    <plurals name="time_hour">
        <item quantity="one">%d hour</item>
        <item quantity="other">%d hours</item>
    </plurals>

    <string name="user_list_add_user_name_placeholder">Your Name</string>
    <plurals name="user_list_blocks">
        <item quantity="one">one block</item>
        <item quantity="other">%d blocks</item>
    </plurals>

    <string name="user_list_edit_difficulty">Configure difficulty</string>
    <string name="user_list_delete_user">Delete user</string>
    <string name="user_list_deleted_user">User deleted</string>
    <string name="user_list_delete_user_question">Do you want to delete %s?</string>
    <string name="user_list_view_statistic">View statistic</string>
    <string name="user_list_create_user">Create user</string>
    <string name="user_list_create_user_confirm_btn">Create</string>
    <string name="user_list_created_user">User %1$s created</string>
    <string name="user_list_create_user_no_name_error">The name must not be empty</string>

    <string name="training_keyboard_ok" translatable="false">OK</string>
    <plurals name="training_result_mistakes">
        <item quantity="one">%d mistake</item>
        <item quantity="other">%d mistakes</item>
    </plurals>
    <plurals name="training_result_seconds">
        <item quantity="one">%d second</item>
        <item quantity="other">%d seconds</item>
    </plurals>
    <plurals name="training_block_result_too_much_mistakes">
        <item quantity="one">%d mistake is too much</item>
        <item quantity="other">%d mistakes are too much</item>
    </plurals>
    <string name="training_block_result_too_slow">You were too slow\nYou made %1$s in %2$s</string>
    <string name="training_block_result_perfect">Perfect, no mistakes</string>
    <plurals name="training_block_result_good">
        <item quantity="one">One mistake is OK</item>
        <item quantity="other">%d mistakes are OK</item>
    </plurals>
    <string name="training_block_result_redo">Repeat the tasks</string>
    <string name="training_block_result_new_tasks">Continue with new tasks</string>
    <string name="training_block_result_stop">Exit</string>
    <string name="training_result_difficulty_tip">You can configure
        the difficulty by holding a user in the user list.
    </string>

    <string name="difficulty_general_settings">General</string>
    <string name="difficulty_operation_addition">Addition</string>
    <string name="difficulty_operation_subtraction">Subtraction</string>
    <string name="difficulty_operation_multiplication">Multiplication</string>
    <string name="difficulty_operation_factorization">Prime factorization</string>
    <string name="difficulty_operation_division">Division</string>
    <string name="difficulty_enable_ten_transgression">Ten transgression</string>
    <string name="difficulty_allow_negative_results">Allow negative results</string>
    <string name="difficulty_allow_remainder">Enable tasks with remainder</string>
    <string name="difficulty_digits_of_first_number">Digits of the first operand</string>
    <string name="difficulty_digits_of_second_number">Digits of the second operand</string>
    <string name="difficulty_profile">Difficulty profile</string>
    <string name="difficulty_operation_disabled">Disabled</string>
    <plurals name="difficulty_digit_counter">
        <item quantity="one">%d digit</item>
        <item quantity="other">%d digits</item>
    </plurals>
    <string name="difficulty_operation_multiplication_enabled">%s times %s</string>
    <string name="difficulty_operation_addition_enabled">%s plus %s %s</string>
    <string name="difficulty_operation_subtraction_enabled">%s minus %s %s</string>
    <string name="difficulty_operation_factorization_enabled">from %d to %d</string>
    <string name="difficulty_operation_division_enabled">up to %d/%d %s</string>
    <string name="difficulty_operation_first_number_digits">The first operand has %s</string>
    <string name="difficulty_operation_second_number_digits">The second operand has %s</string>
    <string name="difficulty_operation_min_number">smallest number</string>
    <string name="difficulty_operation_max_number">biggest number</string>
    <string name="difficulty_operation_enable_switch">Enable</string>
    <string name="difficulty_operation_cannot_disable_reason">This operation must keep enabled,
        because there must be always at least one enabled operation.</string>
    <string name="difficulty_max_divisor">Divisors up to %d</string>
    <string name="difficulty_max_dividend">Dividends up to %d</string>
    <string name="suffix_with_ten_transgression">with ten transgression</string>
    <string name="suffix_without_ten_transgression">without ten transgression</string>
    <string name="suffix_with_negative_results">with negative results</string>
    <string name="suffix_with_remainder">with remainder</string>
    <string name="suffix_without_remainder">without remainder</string>
    <string name="difficulty_profile_easy">easy</string>
    <string name="difficulty_profile_medium">medium</string>
    <string name="difficulty_profile_hard">hard</string>
    <string name="difficulty_profile_custom">custom</string>
    <plurals name="difficulty_tasks_per_block">
        <item quantity="one">%d task per round</item>
        <item quantity="other">%d tasks per round</item>
    </plurals>
    <string name="difficulty_time_per_round">%s per round</string>

    <plurals name="difficulty_take_wrong_tasks">
        <item quantity="other">Ask again for %d wrong answered tasks</item>
        <item quantity="one">Ask again for %d wrong answered task</item>
    </plurals>
    <plurals name="difficulty_take_right_tasks">
        <item quantity="other">Ask again for %d correct answered tasks</item>
        <item quantity="one">Ask again for %d correct answered task</item>
    </plurals>
    <plurals name="difficulty_max_wrong_tasks">
        <item quantity="other">Create new tasks if there were not more than %d wrong answers</item>
        <item quantity="one">Create new tasks if there was not more than %d wrong answer</item>
    </plurals>
    <plurals name="difficulty_general_summary_tasks">
        <item quantity="other">%d tasks</item>
        <item quantity="one">%d task</item>
    </plurals>
    <plurals name="difficulty_general_summary_mistake">
        <item quantity="other">%d mistakes</item>
        <item quantity="one">%d mistake</item>
    </plurals>
    <string name="difficulty_general_summary">%s; %s; %s; repeat %d correct, %d wrong answered tasks</string>
    <string name="difficulty_save_toast">Configuration saved</string>

    <string name="about_title">About this application</string>
    <string name="about_version" translatable="false">Version: %s</string>
    <string name="about_contained_software_title">contained Software</string>
    <string name="about_contained_software_text" translatable="false">
        Kotlin Standard Library
        (<a href="https://github.com/JetBrains/kotlin/blob/master/license/LICENSE.txt">Apache License, Version 2.0</a>)
        \nKotlin Coroutines
        (<a href="https://github.com/JetBrains/kotlin/blob/master/license/LICENSE.txt">Apache License, Version 2.0</a>)
        \nAndroid X
        (<a href="https://www.apache.org/licenses/LICENSE-2.0">Apache License, Version 2.0</a>)
        \nAndroid Room
        (<a href="https://www.apache.org/licenses/LICENSE-2.0">Apache License, Version 2.0</a>)
    </string>
    <string name="about_developer_key_title">Developer PGP key fingerprint</string>
    <string name="about_developer_key_text" translatable="false">2E5C 672D E893 055D 04F5 B7BC 36B4 49FB 5364 BDC4</string>
    <string name="about_source_title">Source code</string>
    <string name="about_source_text" translatable="false">
        <a href="https://codeberg.org/jonas-l/rechnen-android">https://codeberg.org/jonas-l/rechnen-android</a>
    </string>
    <string name="about_license_title">License</string>
    <string name="about_license_text" translatable="false">
        Calculate Android Copyright &#169; 2020 - 2022 Jonas Lochmann
        \nThis program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation version 3 of the License.

        \n\nThis program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        \n\nYou should have received a copy of the GNU General Public License
        along with this program. If not, see <a href="https://www.gnu.org/licenses/">https://www.gnu.org/licenses/</a>.
    </string>

    <plurals name="statistic_solved_tasks">
        <item quantity="one">%d task solved</item>
        <item quantity="other">%d tasks solved</item>
    </plurals>
    <string name="statistic_recent_title">Recently solved tasks</string>
    <string name="statistic_solved_right_percent">%d%% solved correctly</string>

    <string name="statistic_average_duration_per_correct_task_at">Average duration per correct %s</string>
    <string name="statistic_percentage_of_correctly_solved_tasks">Percentage of the correctly solved tasks at the %s</string>

    <string name="statistic_source_before_this_session">Before this session</string>
    <string name="statistic_source_this_session_before_this_round">This session before this round</string>
    <string name="statistic_source_this_round">In this round</string>

    <string name="statistic_value_seconds">seconds</string>

    <string name="input_config_title">Configure keyboard</string>
    <string name="input_config_size">Keyboard size</string>
    <string name="input_config_type">Keyboard type</string>
    <string name="input_config_type_phone">Phone</string>
    <string name="input_config_type_calculator">Calculator</string>
    <string name="input_config_align_horizontal">Horizontal alignment</string>
    <string name="input_config_align_vertical">Vertical alignment</string>
    <string name="input_config_confirm_button_location">OK button location</string>
    <string name="input_config_align_top">Top</string>
    <string name="input_config_align_bottom">Bottom</string>
    <string name="input_config_align_left">Left</string>
    <string name="input_config_align_right">Right</string>
    <string name="input_config_align_center">Center</string>

    <string name="time_trial_mode">Time trial</string>
    <string name="time_trial_result_headline">%s done; %s were solved correctly</string>
</resources>
